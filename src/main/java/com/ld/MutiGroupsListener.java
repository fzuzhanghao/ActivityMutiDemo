package com.ld;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

public class MutiGroupsListener implements TaskListener {

	@Override
	public void notify(DelegateTask arg0) {
		System.out.println("delegateTask.getEventName() = " + arg0.getEventName());
		System.out.println("flag="+arg0.getVariable("passflag"));
		System.out.println(arg0.getFormKey());
		if(arg0.getVariable("passCount")!=null){
			arg0.setVariable("passCount", 0);
		}
		if(arg0.getVariable("totalCount")!=null){
			arg0.setVariable("totalCount", 0);
		}
         //添加会签的人员，所有的都审批通过才可进入下一环节
        List<String> assigneeList = new ArrayList<String>();
        assigneeList.add("kermit");
        assigneeList.add("gonzo");
        arg0.setVariable("partyList",assigneeList);
	}

}
