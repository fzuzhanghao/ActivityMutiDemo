package team.zh.generator;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.junit.Test;


public class DeployFlow {
	
	@Test
	public void deployFlow(){
		ProcessEngine processEngine =ProcessEngineConfiguration.createProcessEngineConfigurationFromResource("activiti.cfg.xml").buildProcessEngine();
		DeploymentBuilder db = processEngine.getRepositoryService().createDeployment();
		Deployment dm = db.name("upgradeApply2").addClasspathResource("team/zh/diagrams/MyProcess.bpmn").addClasspathResource("team/zh/diagrams/MyProcess.png").deploy();
		System.out.println(dm.getId()+":"+dm.getName());
	}

}
